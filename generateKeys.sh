#!/bin/bash

# Configure session environment variables
cd /etc/openvpn/easy-rsa
source ./vars

# Generate server Certificate
./clean-all
./build-ca

# Generate server Keys
./build-key-server rpivpnserver

# Generate sample user called 'nicosingh'
./build-key-pass nicosingh
openssl rsa -in keys/nicosingh.key -des3 -out keys/nicosingh.3des.key

# Generate Diffie-Hellman key exchange
./build-dh

# Generate static HMAC key
openvpn --genkey --secret keys/ta.key

# Copy generated keys to exposed volume (to be used later)
cp -r /etc/openvpn/easy-rsa/keys/* /etc/openvpn/easy-rsa/rpivpnserver-keys/
