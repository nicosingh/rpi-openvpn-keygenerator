FROM resin/rpi-raspbian:stretch-20180214

MAINTAINER Nicolas Singh <nicolas.singh@gmail.com>

# Download required software
RUN apt-get -y update && \
  apt-get -y install openvpn easy-rsa openssl && \
  rm -rf /var/lib/apt/lists/*

# Prepare Easy-RSA configuration
RUN mkdir -p /etc/openvpn/easy-rsa && cp /usr/share/easy-rsa/* /etc/openvpn/easy-rsa
RUN sed -i "s/EASY_RSA=\"\`pwd\`\"/EASY_RSA=\"\/etc\/openvpn\/easy-rsa\"/g" /etc/openvpn/easy-rsa/vars

# Expose output Keys volume
VOLUME /etc/openvpn/easy-rsa/rpivpnserver-keys

# Run script to generate server and client Keys
COPY generateKeys.sh /etc/openvpn/easy-rsa/generateKeys.sh
RUN chmod +x /etc/openvpn/easy-rsa/generateKeys.sh
CMD /etc/openvpn/easy-rsa/generateKeys.sh
