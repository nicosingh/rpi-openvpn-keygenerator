[![pipeline status](https://gitlab.com/nicosingh/rpi-openvpn-keygenerator/badges/master/pipeline.svg)](https://gitlab.com/nicosingh/rpi-openvpn-keygenerator/commits/master) [![Docker Pulls](https://img.shields.io/docker/pulls/nicosingh/rpi-openvpn-keygenerator.svg)](https://hub.docker.com/r/nicosingh/rpi-openvpn-keygenerator/)

# About

Docker image of Raspbian prepared to create OpenVPN server and client keys.

**IMPORTANT**: This image only creates OpenVPN server/client keys, it does not include the OpenVPN server itself. For OpenVPN server image, see: [rpi-openvpn-server](https://hub.docker.com/r/nicosingh/rpi-openvpn-server/)

# How to use this Docker image?

`docker run -it -v my_local_keys:/etc/openvpn/easy-rsa/rpivpnserver-keys nicosingh/rpi-openvpn-keygenerator`

Where:

`-it`: Means 'interactive mode' and is required because OpenVPN Key generators will ask lots of questions (organization, server name, password, ...).

`-v my_local_keys:/etc/openvpn/easy-rsa/rpivpnserver-keys`: Is a good idea to map a volume to a physical folder (`my_local_keys`) to fetch our generated keys when the container ends

**Note**: have in mind the execution of this container might take a long time, since Diffie-Hellman key generation can take a while. In my case it took 30 minutes with a Raspberry Pi 3.
